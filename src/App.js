import React from 'react';
import './App.less';
import Menu from "./Menu";
import Search from './Search';
import Footer from "./Footer";

const App = () => {
  return (
    <div className='App'>
      <Menu />
      <Search />
      <Footer name="Hong Kong"/>
    </div>
  );
};

export default App;