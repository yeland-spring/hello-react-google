import React from "react";

class Buttons extends React.Component{
  render() {
    return (
      <div className="center">
        <button type="submit">Google Search</button>
        <button type="submit">I'm Feeling Lucky</button>
      </div>
    )
  }
}

export default Buttons;