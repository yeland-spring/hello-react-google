import React from 'react';
import './Search.less';
import Input from "./Input";
import Buttons from "./Buttons";

const Search = () => {
  return (
    <section className='search'>
      <header>
        <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
             alt="Google Logo"/>
        <Input />
        <Buttons/>
        <p className="offered">Google offered in: <a href="">中文（繁體）</a> <a href="">中文（简体）</a></p>
      </header>
    </section>
  );
};

export default Search;