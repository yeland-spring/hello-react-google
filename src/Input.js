import React from "react";
import {MdSearch} from "react-icons/md";
import {MdSettingsVoice} from "react-icons/md";

class Input extends React.Component{
  constructor(props, context) {
    super(props, context);
    this.state = {
      text: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  render() {
    return (
      <div>
        <div><button onClick={this.handleClick}/></div>
        <input type="text" onChange={this.handleChange}/>
        <MdSearch className="searchIcon icon"/>
        <MdSettingsVoice className="voiceIcon icon"/>
      </div>
    )
  }

  handleChange(event) {
    this.setState({text: event.target.value});
  }

  handleClick() {
    alert(this.state.text);
  }
}

export default Input;