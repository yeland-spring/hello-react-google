import React from "react";
import "./Footer.less";

const Footer = (props) => {
  return (
    <footer>
      <section className="above basic">{props.name}</section>
      <nav className="below basic">
        <ul className="left">
          <li><a href="">Advertising</a></li>
          <li><a href="">Business</a></li>
          <li><a href="">About</a></li>
          <li><a href="">How Search works</a></li>
        </ul>
        <ul className="right">
          <li><a href="">Privacy</a></li>
          <li><a href="">Terms</a></li>
          <li><a href="">Settings</a></li>
        </ul>
      </nav>
    </footer>
  )
}

export default Footer;